=begin
{
  "chatwork": {
    "apikey": "API KEY",
    "roomid": "ROOM ID"
  }
}
=end

require 'uri'
require 'net/http'
require 'net/https'

if node[:chatwork] and node[:chatwork][:apikey] and node[:chatwork][:roomid]

  uri = URI.parse("http://169.254.169.254/latest/meta-data/public-hostname")
  public_dns_name = Net::HTTP.get(uri)

  uri = URI.parse("https://api.chatwork.com/v1/rooms/#{node[:chatwork][:roomid]}/messages")
  https = Net::HTTP.new(uri.host, uri.port)
  https.use_ssl = true
  req = Net::HTTP::Post.new(uri.path, initheader = {'X-ChatWorkToken' => node[:chatwork][:apikey]})
  req.body = "body=[info][title]Wake Up (flex)[/title]Redmine\npublic_dns_name: #{public_dns_name}[/info]"
  res = https.request(req)

else
  Chef::Log.warn("if you want to send message to Chatwork, set up Custom JSON please.")
end

