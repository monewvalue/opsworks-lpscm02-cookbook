lnk_paths = %w{ /usr/share/tomcat7/.jenkins }
mnt_paths = %w{ /mnt/jenkins }
owners = %w{ tomcat }

lnk_paths.zip(mnt_paths, owners).each do |path, mpath, owner|

  execute "mv #{path}" do
    command <<-EOH
      mv #{path} #{path}_back
    EOH
    not_if "test -L #{path}"
  end

  directory mpath do
    recursive true
    action :create
    not_if { File.exists?(mpath) }
  end

  link path do
    to mpath
    link_type :symbolic
    not_if "test -L #{path}"
  end

  directory path do
    owner owner
    group owner
    recursive true
  end

end

