opsworks-lpscm-cookbooks
=========================

LogPlatform SCMを構築するための、OpsWorks用Cookbooks。

OpsWorksのcustom Chef cookbooksとして、このリポジトリを登録して使用できます。

Layersの各タイミング(Setup/Configure/Deploy/Undeploy/Shutdown)に合わせてレシピを指定し、実行させることが可能です。

Stack Settings時に、Custom JSONとして、ChatworkのAPIキーや、roomidを指定しないとChatworkにメッセージを送ることができません。

Chatworkに流したいメッセージを任意に変更するには、いまのところ、ベタっと書いてあるコードのほうを修正する手段しか提供できていません。
