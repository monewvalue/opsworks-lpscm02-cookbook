include_recipe 'deploy'

link "/home/ubuntu/redmine-2.6.3-0" do
  to "/mnt/redmine-2.6.3-0"
  link_type :symbolic
  not_if "test -L /home/ubuntu/redmine-2.6.3-0"
end

execute "sudo -u ubuntu /home/ubuntu/redmine-2.6.3-0/ctlscript.sh start"

