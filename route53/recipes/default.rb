require 'uri'

uri = URI.parse("http://169.254.169.254/latest/meta-data/public-ipv4")
public_ipv4 = Net::HTTP.get(uri)

dns_list = node["scm"]["dns_list"]

dns_list.each do |dns|
  hosted_zone_id = dns.hosted_zone_id
  dns_name = dns.name

  execute "change-resource-record-sets" do
    command <<-EOH
      aws route53 change-resource-record-sets --hosted-zone-id #{hosted_zone_id} --change-batch '{"Changes":[{"Action":"UPSERT","ResourceRecordSet":{"Name":"#{dns_name}","Type":"A","TTL":300,"ResourceRecords":[{"Value":"#{public_ipv4}"}]}}]}'
    EOH
    user dns.user
    group dns.group
  end
end

